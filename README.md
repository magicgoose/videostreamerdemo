
###Android versions

The app should run on phones and tablets with Android 4.0+. Tested on Android 5.0 and 6.0.

###How to build

Run `./gradlew assembleDebug`, then look for apk in `./app/build/outputs/apk`.  
Alternatively, use standalone gradle (2.10) and/or Android Studio (2.0), however, IDE is not necessary for building.  

The machine should have Java Runtime and up-to-date Android SDK installed, the build should work with JDK 1.8.*.

An already built .apk can be also downloaded [here](https://drive.google.com/file/d/0BwzrN1KmDAUDTGIydHFHNTM1Tzg/view?usp=sharing).