package magicgoose.videostreamerdemo;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class TheApp extends Application {
    public static final String VIDEO_URL = "http://alohabrowser.com/tmp/test_video.mp4";
    private static final String DOWNLOAD_FILENAME = "test_video.mp4";

    private static TheApp INSTANCE;
    private File downloadPath;

    private volatile int progress;
    private final Handler handler = new Handler(Looper.getMainLooper());

    private final Runnable doReportDoneRunnable = new Runnable() {
        @Override
        public void run() {
            doReportDone();
        }
    };

    private final Runnable doReportProgressRunnable = new Runnable() {
        @Override
        public void run() {
            doReportProgress();
        }
    };

    private final Runnable doReportFailRunnable = new Runnable() {
        @Override
        public void run() {
            doReportFail();
        }
    };
    private VideoDownloadListener listener;
    private boolean isDownloading;


    public static TheApp getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        downloadPath = new File(getCacheDir(), DOWNLOAD_FILENAME); // at least this always exists
    }

    public void startVideoDownload() {
        if (isDownloading) {
            return;
        }

        isDownloading = true;

        final Thread downloadThread = new Thread(new Runnable() {
            @Override
            public void run() {
                doDownload();
            }
        });
        downloadThread.setPriority(Thread.MIN_PRIORITY);
        downloadThread.start();
    }

    private void doDownload() {
        try {
            downloadPath.createNewFile();
            reportProgress(0);
            URL url = new URL(VIDEO_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            while (responseCode == 301) {
                final String location = connection.getHeaderField("Location");
                url = new URL(new URL(url, location).toExternalForm());

                connection = (HttpURLConnection) url.openConnection();
                connection.setInstanceFollowRedirects(true);
                connection.connect();
                responseCode = connection.getResponseCode();
            }

            final int totalSize = connection.getContentLength();

            final InputStream input = new BufferedInputStream(connection.getInputStream());
            final OutputStream output = new FileOutputStream(downloadPath);

            final byte buffer[] = new byte[32 * 1024];
            long totalDownloadedSize = 0;
            int size;
            while ((size = input.read(buffer)) != -1) {
                totalDownloadedSize += size;
                final int progress = (int) (totalDownloadedSize * 100 / totalSize);
                reportProgress(progress);
                output.write(buffer, 0, size);
            }

            output.flush();
            output.close();
            input.close();

            reportDone();
        } catch (IOException e) {
            e.printStackTrace();
            reportFail();
            downloadPath.delete();
        }
    }

    private void reportFail() {
        isDownloading = false;
        handler.post(doReportFailRunnable);
    }

    private void reportDone() {
        isDownloading = false;
        handler.post(doReportDoneRunnable);
    }

    private void reportProgress(int progress) {
        if (this.progress != progress) {
            this.progress = progress;
            handler.post(doReportProgressRunnable);
        }
    }

    public boolean isVideoDownloaded() {
        return !isDownloading && downloadPath.exists();
    }

    public File getLocalVideoPath() {
        if (!isVideoDownloaded())
            throw new UnsupportedOperationException();

        return downloadPath;
    }

    private void doReportDone() {
        listener.onDownloadDone();
    }
    private void doReportProgress() {
        listener.onDownloadProgress(this.progress);
    }
    private void doReportFail() {
        listener.onDownloadFail();
    }

    public void setListener(VideoDownloadListener listener) {
        this.listener = listener;
        if (isDownloading && listener != null) {
            listener.onDownloadProgress(progress);
        }
    }
}
