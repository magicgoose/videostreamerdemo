package magicgoose.videostreamerdemo;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;



public class MainActivity extends AppCompatActivity implements VideoDownloadListener {


    private static final int THUMBNAIL_TIME_MS = 100;
    private static final String KEY_IS_PLAYING = "KEY_IS_PLAYING";
    private static final String KEY_CURRENT_POSITION = "KEY_CURRENT_POSITION";

    private VideoView theVideoView;

    private PlayerState playerState = PlayerState.STOPPED;
    private TextView playButton;
    private TextView downloadButton;
    private ProgressBar progressBar;
    private TextView progressText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        theVideoView = (VideoView) findViewById(R.id.theVideoView);
        playButton = (TextView) findViewById(R.id.playButton);
        downloadButton = (TextView) findViewById(R.id.downloadButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressText = (TextView) findViewById(R.id.progressText);

        resetVideoSource(theVideoView);

        updateButtonsLook();

        if (savedInstanceState != null) {
            restorePlayerState(savedInstanceState);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TheApp.getInstance().setListener(this);
    }

    @Override
    protected void onPause() {
        TheApp.getInstance().setListener(null);
        super.onPause();
    }

    private void updateButtonsLook() {
        final boolean isVideoDownloaded = TheApp.getInstance().isVideoDownloaded();

        downloadButton.setEnabled(!isVideoDownloaded);

        if (!isPlaying() && isVideoDownloaded) {
            playButton.setText(R.string.playLocal);
        } else if (!isPlaying()) {
            playButton.setText(R.string.play);
        } else {
            playButton.setText(R.string.stop);
        }
    }

    private boolean isPlaying() {
        return playerState != PlayerState.STOPPED;
    }

    private void resetVideoSource(VideoView theVideoView) {
        final TheApp app = TheApp.getInstance();

        if (app.isVideoDownloaded()) {
            theVideoView.setVideoURI(Uri.fromFile(app.getLocalVideoPath()));
        } else {
            theVideoView.setVideoURI(Uri.parse(TheApp.VIDEO_URL));
        }
        updateButtonsLook();
        theVideoView.seekTo(THUMBNAIL_TIME_MS);
    }

    public void onPlayButtonClick(View view) {
        setPlayingState(isPlaying() ?
                        PlayerState.STOPPED :
                        PlayerState.PLAYING,
                0);
    }

    public void onDownloadButtonClick(View view) {
        TheApp.getInstance().startVideoDownload();
    }

    private void setPlayingState(PlayerState newPlayerState, int startTime) {
        playerState = newPlayerState;
        updateButtonsLook();

        switch (playerState) {
            case STOPPED:
                theVideoView.stopPlayback();
                resetVideoSource(theVideoView);
                break;
            case PLAYING:
                theVideoView.seekTo(startTime);
                theVideoView.start();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final boolean isPlaying = theVideoView.isPlaying();
        final int currentPosition = isPlaying ? theVideoView.getCurrentPosition() : 0;

        savePlayerState(outState, isPlaying, currentPosition);
    }

    private void savePlayerState(Bundle outState, boolean isPlaying, int currentPosition) {
        outState.putBoolean(KEY_IS_PLAYING, isPlaying);
        outState.putInt(KEY_CURRENT_POSITION, currentPosition);
    }

    private void restorePlayerState(Bundle inState) {
        final boolean isPlaying = inState.getBoolean(KEY_IS_PLAYING, false);
        final int position = inState.getInt(KEY_CURRENT_POSITION, THUMBNAIL_TIME_MS);
        setPlayingState(isPlaying ? PlayerState.PLAYING : PlayerState.STOPPED, position);
    }

    private void showProgress(int progress) {
        progressBar.setProgress(progress);
        progressText.setText("" + progress + "%");
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
    }

    private void removeProgressBar() {
        progressBar.setVisibility(View.GONE);
        progressText.setVisibility(View.GONE);
    }

    @Override
    public void onDownloadDone() {
        Toast.makeText(this, "Download done!", Toast.LENGTH_SHORT).show();
        resetVideoSource(theVideoView);
        removeProgressBar();
    }

    @Override
    public void onDownloadProgress(int progress) {
        showProgress(progress);
    }

    @Override
    public void onDownloadFail() {
        Toast.makeText(this, "Download failed!", Toast.LENGTH_SHORT).show();
        removeProgressBar();
    }
}
