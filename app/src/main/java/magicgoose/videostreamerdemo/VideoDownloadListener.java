package magicgoose.videostreamerdemo;

public interface VideoDownloadListener {
    void onDownloadDone();

    void onDownloadProgress(int progress);

    void onDownloadFail();
}
